# skalibs - a general-purpose low-level C library
-----------------------------------------------

 skalibs is a C library used by all the skarnet.org packages.
It provides APIs more suited to secure and efficient system
programming than the libc, especially where network programming
and IPCs are concerned.

 See the enclosed doc/ subdirectory for the documentation, or
 https://skarnet.org/software/skalibs/


## Installation
  ------------

 See the INSTALL file.

## Recommended build instructions

The recommended method to build this package directly from git.
```
gbp clone https://gitlab.com/init-diversity/s6-rc/skalibs.git && 
cd skalibs && 
gbp buildpackage -uc -us
```

The following should get you all the software required to build using this method:

```
sudo apt install git-buildpackage dpkg-dev
```

## Customization

You can customize paths via flags given to configure. See `./configure --help` for a list of all available configure options.

These flags will need to be added to the `debian/rules` file.



* Contact information
  -------------------

 Laurent Bercot <ska-skaware at skarnet.org>

 Please use the <skaware at list.skarnet.org> mailing-list for
questions about skalibs.
